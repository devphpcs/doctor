from django.urls import path,include
from.import views
urlpatterns = [
    path("sign_up/",views.signup,name="signup"),
    path("sign_in/",views.login,name="login"),
    path("get_my_accoount/<slug:id>/",views.get_my_accoount,name="get_my_"),
    path("edit_doctor/<slug:id>/",views.edit_doctor,name="edit_doctor"),
    path("create_patient/",views.create_patient,name="create_patient"),
    path("set_my_availavle_slot/<str:id>/",views.set_my_availavle_slot,name="set_my_availavle_slot"),
    path("get_my_availavle_slot/<slug:id>/",views.get_my_availavle_slot,name="get_my_availavle_slot"),


]