from django.shortcuts import render, HttpResponse
from .models import *
from .serializers import *
from rest_framework import views,response,status,viewsets,permissions, generics
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from django.http import JsonResponse
from django.contrib.auth import get_user_model 
from rest_framework.views import APIView
from django.contrib.auth import authenticate,login,get_user_model,logout
from django.contrib.auth.hashers import make_password
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from rest_framework.request import Request
from rest_framework import status
from rest_framework.status import (HTTP_400_BAD_REQUEST,HTTP_404_NOT_FOUND,HTTP_200_OK)
from rest_framework.permissions import IsAuthenticated 
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import secrets
import string
# Create your views here.
@api_view(['post'])
@csrf_exempt
def signup(request):
    if request.method == 'POST':
        doctor = DoctorSerializer(data = request.data)
        if doctor.is_valid():
            demo = doctor.save()
            demoid=demo.id
            try:
                token = Token.objects.get(user_id=demoid)
            except:
                token = Token.objects.create(user_id=demoid)
            return Response({'message':'Doctor registered Successfully'},status=status.HTTP_201_CREATED)

@api_view(['post'])
@csrf_exempt
def login(request):
    if request.method == 'POST':
        phone = request.data.get('phone')
        user = User.objects.get(phone=phone)
        if str(user.phone) == str(phone):
            res = ''.join(secrets.choice(string.ascii_uppercase + string.digits)for i in range(28))
            token = Token.objects.update(key=res)
            return Response({"token" : str(res), "message" : "login successfully"},status=HTTP_200_OK)
        return Response("HTTP_404_NOT_FOUND")
    else:
        return Response({"Message" : "Wrong input try again"},status=HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_my_accoount(request,id):
    token = Token.objects.get(key=id)
    user = User.objects.get(id=token.user_id)
    serializer = DoctorSerializer(user)
    json_data = JSONRenderer().render(serializer.data)
    return HttpResponse(json_data, content_type='application/json')

@api_view(["PUT"])
def edit_doctor(request, id):
    token = Token.objects.get(key=id)
    doctor = User.objects.get(pk=token.user_id)
    serializer = DoctorSerializer(doctor, data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors,status = 400)

@api_view(['post'])
def create_patient(request):
        if request.method == 'POST':
            patient = PatientSerializer(data = request.data)
            if patient.is_valid():
                demo = patient.save(commit=False)
                demo.user_id = request.user.id
                demo.save()
                return Response({'message':'Patient Created Successfully'},status=status.HTTP_201_CREATED)

@api_view(['post'])
def set_my_availavle_slot(request,id):
    instance = Token.objects.get(key=id)
    if request.method == 'POST':
        schedule = ScheduleSerializer(data = request.data)
        if schedule.is_valid():
            schedule.save()
            return Response({'message':'Slot Created Successfully'},status=status.HTTP_201_CREATED)
        return Response({"Message" : "Something went wrong"},status=HTTP_400_BAD_REQUEST)
        
@api_view(["GET"])
def get_my_availavle_slot(request, id):
    tok = Token.objects.get(key=id)
    user = Schedule.objects.get(id=tok.user_id)
    serializer = ScheduleSerializer(user)
    json_data = JSONRenderer().render(serializer.data)
    return HttpResponse(json_data, content_type='application/json')
