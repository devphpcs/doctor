from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib import auth
from django.contrib.auth.models import AbstractUser
# Create your models here.

TIMESLOT_LIST = (
    (0, '09:00 – 09:30'),
    (1, '10:00 – 10:30'),
    (2, '11:00 – 11:30'),
    (3, '12:00 – 12:30'),
    (4, '13:00 – 13:30'),
    (5, '14:00 – 14:30'),
    (6, '15:00 – 15:30'),
    (7, '16:00 – 16:30'),
    (8, '17:00 – 17:30'),
)

FROM_dAY_TO_DAY = (
    (1, 'sunday – sunday'),
    (2, 'monday – friday'),
    (3, 'sunday – monday'),
)


class User(AbstractUser):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=245)
    phone = models.IntegerField()
    address = models.CharField(max_length=100)
    STATUS = ((0, "Online"),(1, "Clinical"))
    consultation_type = models.PositiveIntegerField(choices=STATUS, default=0)
    consultation_price = models.CharField(max_length=100)
    specialities = models.CharField(max_length=100)
    insurance_company = models.CharField(max_length=100)
    insurance_number = models.CharField(max_length=50)
    class Meta:
        managed = True;
        db_table = 'tbl_doctor'

class Patient(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    phone = models.IntegerField()
    address = models.CharField(max_length=100)
    class Meta:
        managed = True;
        db_table = 'tbl_patient'

class Schedule(models.Model):
    doctor = models.ForeignKey(User,on_delete=models.CASCADE)
    oopen = models.TimeField()
    close = models.TimeField()
    from_date = models.DateField()
    to_date = models.DateField()
    from_time_to_time = models.PositiveIntegerField(choices=TIMESLOT_LIST, default=1)
    from_day_to_day = models.PositiveIntegerField(choices=FROM_dAY_TO_DAY, default=2)
  
    class Meta:
        managed = True;
        db_table = 'tbl_schedule'

