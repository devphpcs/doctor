from rest_framework import serializers
from .models import User, Patient, Schedule

class DoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','name','email','phone','address','consultation_type','consultation_price','specialities','insurance_company','insurance_number')

class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = '__all__'

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'